package com.km.testio;

public class Location {
    public String name;
    public Integer distance;

    public Location(String name, Integer distance) {
        this.name = name;
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public Integer getDistance() {
        return distance;
    }
}
