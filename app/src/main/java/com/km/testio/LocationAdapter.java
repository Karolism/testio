package com.km.testio;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Collections;
import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationHolder> {
    private LayoutInflater inflater;
    List<LocationModel> data= Collections.emptyList();

    public LocationAdapter(Context context, List<LocationModel> data){
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @NonNull
    @Override
    public LocationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_row, parent, false);

        LocationHolder holder = new LocationHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull LocationHolder holder, int position) {
        LocationModel location = data.get(position);
        holder.name.setText(location.name);
        holder.distance.setText(location.distance + "km");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class LocationHolder extends RecyclerView.ViewHolder{
        TextView name;
        TextView distance;
        public LocationHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            distance = itemView.findViewById(R.id.distance);
        }
    }
}
