package com.km.testio;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.ContentValues.TAG;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */

public class ListActivity extends Activity {
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    @BindView(R.id.recycleView) RecyclerView recyclerView;
    @BindView(R.id.include) Toolbar toolbar;
    @BindView(R.id.refresh) SwipeRefreshLayout swipeRefreshLayout;
    private LocationAdapter locationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list);
        ActiveAndroid.initialize(this);
        ButterKnife.bind(this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });

        List<LocationModel> storedLocations = LocationModel.getAllLocations();
        locationAdapter = new LocationAdapter(this,storedLocations);

        recyclerView.setAdapter(locationAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));



        Log.d("Stored", "onCreate: ");
    }

    private void refreshData() {

        Gson gson = new GsonBuilder().serializeNulls().disableInnerClassSerialization().create();
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(API.BASE_URL);
        builder.addConverterFactory(GsonConverterFactory.create(gson));
        Retrofit retrofit = builder.build();

        API api = retrofit.create(API.class);
        SessionManager sessionManager = new SessionManager(getApplicationContext());

        Call<List<Location>> call = api.locations("Bearer " + sessionManager.getToken());

        call.enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(Call<List<Location>> call, Response<List<Location>> response) {
                if (response.body() != null) {
                    new Delete().from(LocationModel.class).execute();
                    List<Location> locations = response.body();

                    for (Location l : locations) {
                        LocationModel location = new LocationModel(l.getName(), l.getDistance());
                        location.save();
                    }

                }
            }

            @Override
            public void onFailure(Call<List<Location>> call, Throwable t) {

            }
        });
        swipeRefreshLayout.setRefreshing(false);
        List<LocationModel> storedLocations = LocationModel.getAllLocations();
        locationAdapter = new LocationAdapter(this,storedLocations);

        recyclerView.setAdapter(locationAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    @OnClick(R.id.logout)
    public void onClick(){
        new Delete().from(LocationModel.class).execute();
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        sessionManager.logoutUser();

    }

}


