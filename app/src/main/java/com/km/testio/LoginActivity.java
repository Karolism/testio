package com.km.testio;

import android.app.Activity;
import com.km.testio.LocationModel;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.ContentValues.TAG;

public class LoginActivity extends Activity {

    @BindView(R.id.username) EditText surename;
    @BindView(R.id.password) EditText password;
    @BindView(R.id.login) Button login;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getApplicationContext());
        if(sessionManager.isLoggedIn()){
            Intent i = new Intent(LoginActivity.this, ListActivity.class);
            startActivity(i);
            finish();
        }
        setContentView(R.layout.activity_login);
        ActiveAndroid.initialize(this);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.login)
    public void onClick(){

        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(API.BASE_URL);
        builder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();

        API api = retrofit.create(API.class);

        Call<UserToken> call = api.token(surename.getText(),password.getText());

        call.enqueue(new Callback<UserToken>() {
            @Override
            public void onResponse(Call<UserToken> call, Response<UserToken> response) {
                if(response.body() !=null) {
                    UserToken token = response.body();
                    SessionManager manager = new SessionManager(getApplicationContext());
                    manager.createLoginSession(String.valueOf(surename.getText()), token.getToken());
                    setContentView(R.layout.fetch_layout);
                    getLocations(token.getToken());
                } else {
                  Toast.makeText(getApplicationContext(), R.string.Wrong, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserToken> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t );
            }
        });
    }

    private void getLocations(final String token) {


        Gson gson = new GsonBuilder().serializeNulls().disableInnerClassSerialization().create();
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(API.BASE_URL);
        builder.addConverterFactory(GsonConverterFactory.create(gson));
        Retrofit retrofit = builder.build();

        API api = retrofit.create(API.class);
        Call<List<Location>> call = api.locations("Bearer " + token);

        call.enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(Call<List<Location>> call, Response<List<Location>> response) {
                if(response.body() != null) {
                    List<Location> locations = response.body();

                    for (Location l : locations) {
                        LocationModel location = new LocationModel(l.getName(),l.getDistance());
                        location.save();
                    }
                    Intent i = new Intent(LoginActivity.this, ListActivity.class);
                    startActivity(i);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<List<Location>> call, Throwable t) {

            }
        });
    }
}
