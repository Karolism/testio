package com.km.testio;
public class UserToken{
    private String token;

    public UserToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
