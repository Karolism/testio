package com.km.testio;

import android.text.Editable;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API {
    String BASE_URL = "http://playground.tesonet.lt/v1/";

    @Headers("Content-type: application/json")
    @POST("tokens")
    Call<UserToken> token(@Query("username") Editable username, @Query("password") Editable password);

    @Headers("Content-type: application/json")
    @GET("servers")
    Call<List<Location>> locations(@Header("Authorization") String token);
}
