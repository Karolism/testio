package com.km.testio;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Table(name = "Locations")
public class LocationModel extends Model {

    @Column(name = "name")
    public String name;

    @Column(name = "distance")
    public Integer distance;

    public LocationModel(){
        super();
    }

    public LocationModel(String name, Integer distance) {
        super();
        this.name = name;
        this.distance = distance;
    }

    public static List<LocationModel>getAllLocations(){
        return new Select().from(LocationModel.class).execute();
    }

    public String getName() {
        return name;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }
}
